import React from 'react';
import Expo from 'expo';
import { StyleProvider, Root } from 'native-base';
import { createDrawerNavigator, createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons'; // eslint-disable-line
import Storage from 'react-native-storage';
import { AsyncStorage } from 'react-native';

import CalendarView from './src/screens/timetable/view/Calendar';
import DayView from './src/screens/timetable/view/Day';
import getTheme from './native-base-theme/components';
import GetTimetable from './src/screens/timetable/Get';
import Login from './src/screens/timetable/Login';
import Process from './src/screens/timetable/Process';
import Settings from './src/screens/Settings';
import SideBar from './src/screens/SideBar';
import variables from './native-base-theme/variables/commonColor';


const storage = new Storage({
	size: 1000000,
	storageBackend: AsyncStorage,
});

global.storage = storage;

const ViewNavigator = createBottomTabNavigator({
	Day: {
		screen: DayView,
		navigationOptions: {
			title: 'Day',
			tabBarIcon: ({ focused, tintColor }) => {
				const iconName = `ios-clock${focused ? '' : '-outline'}`;
				return <Ionicons name={iconName} size={25} color={tintColor} />;
			},
		},
	},
	Calendar: {
		screen: CalendarView,
		navigationOptions: {
			title: 'Calendar',
			tabBarIcon: ({ focused, tintColor }) => {
				const iconName = `ios-calendar${focused ? '' : '-outline'}`;
				return <Ionicons name={iconName} size={25} color={tintColor} />;
			},
		},
	},
});

const Drawer = createDrawerNavigator(
	{
		Login: {
			screen: Login,
			navigationOptions: {
				title: 'Get Timetable',
			},
		},
		Settings: {
			screen: Settings,
			navigationOptions: {
				title: 'Settings',
			},
		},
		GetTimetable: {
			screen: GetTimetable,
			navigationOptions: {
				title: 'Get Timetable',
			},
		},
		ViewTimetable: {
			screen: ViewNavigator,
			navigationOptions: {
				title: 'View Timetable',
			},
		},
		Process: {
			screen: Process,
			navigationOptions: {
				title: 'Process Timetable',
			},
		},
	},
	{
		initialRouteName: 'Login',
		contentOptions: {
			activeTintColor: '#e91e63',
		},
		contentComponent: props => <SideBar {...props} />,
	},
);

const AppNavigator = createStackNavigator(
	{
		Drawer: { screen: Drawer },
	},
	{
		initialRouteName: 'Drawer',
		headerMode: 'none',
	},
);

export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: true,
		};
	}
	async componentWillMount() {
		await Expo.Font.loadAsync({
			Roboto: require('native-base/Fonts/Roboto.ttf'), // eslint-disable-line
			Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'), // eslint-disable-line
		});
		this.setState({ loading: false });
	}

	render() {
		if (this.state.loading) {
			return <Expo.AppLoading />;
		}
		return (
			<StyleProvider style={(variables)}>
				<Root>
					<AppNavigator />
				</Root>
			</StyleProvider>
		);
	}
}
