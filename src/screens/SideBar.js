import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native';
import { Content, Text, List, ListItem, Icon, Container, Left } from 'native-base';

const { Dimensions, Platform } = require('react-native');

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = {
	drawerCover: {
		alignSelf: 'stretch',
		height: deviceHeight / 6,
		width: null,
		position: 'relative',
		marginBottom: 10,
	},
	drawerImage: {
		position: 'absolute',
		left: Platform.OS === 'android' ? deviceWidth / 10 : deviceWidth / 9,
		top: Platform.OS === 'android' ? deviceHeight / 13 : deviceHeight / 12,
		width: 210,
		height: 75,
		resizeMode: 'cover',
	},
	text: {
		fontWeight: Platform.OS === 'ios' ? '500' : '400',
		fontSize: 16,
		marginLeft: 20,
	},
};

const drawerImage = require('../images/logo.png');

const datas = [
	{
		name: 'Change Timetable',
		route: 'Login',
		icon: 'ios-calendar',
		bg: '#C5F442',
	},
	{
		name: 'View Timetable',
		route: 'ViewTimetable',
		icon: 'ios-clock',
		bg: '#C5F442',
	},
	{
		name: 'Settings',
		route: 'Settings',
		icon: 'ios-cog',
		bg: '#C5F442',
	},
];

const SideBar = props => (
	<Container>
		<Content
			bounces={false}
			style={{ flex: 1, backgroundColor: '#fff', top: -1 }}
		>
			<Image style={styles.drawerCover} />
			<Image square style={styles.drawerImage} source={drawerImage} />

			<List
				dataArray={datas}
				renderRow={data =>
					(
						<ListItem
							button
							noBorder
							onPress={() => props.navigation.navigate(data.route)}
						>
							<Left>
								<Icon
									active
									name={data.icon}
									style={{ color: '#0F2645', fontSize: 26, width: 30 }}
								/>
								<Text style={styles.text}>
									{data.name}
								</Text>
							</Left>
						</ListItem>
					)
				}
			/>
		</Content>
	</Container>
);


SideBar.propTypes = {
	navigation: PropTypes.any,
};

export default SideBar;
