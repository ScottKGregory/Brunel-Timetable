import React from 'react';
import PropTypes from 'prop-types';
import { Body, H1 } from 'native-base';
import ScreenContainer from '../../../components/ScreenContainer';

const Calendar = props => (
	<ScreenContainer title="Calendar" navigation={props.navigation} padder logo>
		<Body>
			<H1>Calendar View</H1>
		</Body>
	</ScreenContainer>
);

Calendar.propTypes = {
	navigation: PropTypes.any,
};

export default Calendar;
