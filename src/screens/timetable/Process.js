import React from 'react';
import PropTypes from 'prop-types';
import { H1, Body, Spinner } from 'native-base';

import ScreenContainer from '../../components/ScreenContainer';
import WebView from '../../components/WebView';
import { parse } from '../../helpers/GetTimetable';

export default class Get extends React.Component {
	constructor(props) {
		super(props);
		this.state = {

		};

		this.onLoad = this.onLoad.bind(this);
		this.gotTimetable = this.gotTimetable.bind(this);
	}

	onLoad() {
		this.webView.getTimetable();
	}

	gotTimetable(timetable) {
		const tt = parse(timetable);

		this.props.navigation.navigate('ViewTimetable');
	}

	render() {
		return (
			<ScreenContainer title="Processing" navigation={this.props.navigation} padder logo>
				<Body>
					<H1>Loading</H1>
					<Spinner color="#0F2645" />
				</Body>
				<WebView
					url="https://teaching.brunel.ac.uk/SWS-1718/showtimetable.aspx"
					ref={webView => this.webView = webView}
					onLoad={this.onLoad}
					getTimetableCallback={this.gotTimetable}
				/>
			</ScreenContainer>
		);
	}
}

Get.propTypes = {
	navigation: PropTypes.any,
};
