import React from 'react';
import PropTypes from 'prop-types';
import { View, Modal } from 'react-native';
import { Form, Item, Input, Label, Button, Text, CheckBox, Body, ListItem, Card, CardItem, Icon, H1, Spinner } from 'native-base';
import ScreenContainer from '../../components/ScreenContainer';
import WebView from '../../components/WebView';

const { Dimensions } = require('react-native');

const deviceHeight = Dimensions.get('window').height;

export default class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			saveLogin: false,
			modalShow: false,
			idError: false,
			idSuccess: false,
			idText: '',
			passwordError: false,
			passwordSuccess: false,
			passwordText: '',
			webviewLoaded: false,
			autoLogin: false,
		};

		this.toggleSave = this.toggleSave.bind(this);
		this.toggleModal = this.toggleModal.bind(this);
		this.doLogin = this.doLogin.bind(this);
		this.onLoad = this.onLoad.bind(this);
	}

	componentWillMount() {
		global.storage.load({
			key: 'LoginDetails',
			autoSync: true,
			syncInBackground: true,
		}).then((ret) => {
			this.setState({
				...this.state, idText: ret.id, passwordText: ret.password, saveLogin: true, autoLogin: true,
			});
		}).catch((err) => {
			console.warn(err.message);
		});
	}

	componentWillUpdate(nextProps, nextState) {
		if (nextState.autoLogin && nextState.webviewLoaded) {
			this.doLogin();
		}
	}

	onLoad(url) {
		if (url.includes('login.aspx')) {
			this.setState({ ...this.state, webviewLoaded: true });
		} else if (url.includes('default.aspx')) {
			this.props.navigation.navigate('GetTimetable');
		}
	}

	doLogin() {
		let idError = false;
		let passwordError = false;

		if (this.state.idText === '' || this.state.idText === null) {
			idError = true;
		}

		if (this.state.passwordText === '' || this.state.passwordText === null) {
			passwordError = true;
		}

		this.setState({
			...this.state, idError, passwordError, idSuccess: !idError, passwordSuccess: !passwordError,
		});

		if (!idError && !passwordError) {
			if (this.state.saveLogin) {
				global.storage.save({
					key: 'LoginDetails',
					data: {
						id: this.state.idText,
						password: this.state.passwordText,
					},
					expires: null,
				});
			}
			this.setState({ ...this.state, webviewLoaded: false });
			this.webView.doLogin(this.state.idText, this.state.passwordText);
		}
	}

	toggleSave() {
		this.setState({
			...this.state,
			saveLogin: !this.state.saveLogin,
			modalShow: this.state.saveLogin ? this.state.modalShow : !this.state.modalShow,
		});
	}

	toggleModal() {
		this.setState({
			...this.state,
			modalShow: !this.state.modalShow,
		});
	}

	render() {
		return (
			<ScreenContainer title="Login" navigation={this.props.navigation} padder logo>
				{!this.state.webviewLoaded &&
				<Body>
					<H1>Loading</H1>
					<Spinner color="#0F2645" />
				</Body>
				}
				<Modal
					animationType="fade"
					transparent
					visible={this.state.modalShow}
					presentationStyle="overFullScreen"
				>
					<View style={{ paddingTop: deviceHeight / 4, flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
						<Card style={{ flex: 0.43, paddingBottom: 0 }}>
							<CardItem header>
								<H1 style={{ textAlign: 'center', width: '100%' }}>Login warning</H1>
							</CardItem>
							<CardItem cardBody>
								<Body style={{ width: '100%' }}>
									<Text style={{ width: '100%', textAlign: 'center' }}>
										<Icon
											name="ios-warning"
											style={{ fontSize: 72, color: 'orange' }}
										/>
									</Text>
								</Body>
							</CardItem>
							<CardItem>
								<Body>
									<Text style={{ textAlign: 'center' }}>
										Checking this option will store your
										<Text style={{ fontWeight: '600' }}> Student ID</Text> &amp;
										<Text style={{ fontWeight: '600' }}> Password</Text> on this device.
										It is never sent anywhere other than the Brunel Timetable website.
									</Text>
								</Body>
							</CardItem>
							<CardItem>
								<Button block style={{ width: '100%' }} onPress={() => this.toggleModal()}>
									<Text>Accept</Text>
								</Button>
							</CardItem>
						</Card>
					</View>
				</Modal>
				{this.state.webviewLoaded &&
				<View>
					<Form>
						<Item
							error={this.state.idError}
							success={this.state.idSuccess}
							style={{ marginLeft: 0 }}
							floatingLabel
						>
							<Label>Student ID</Label>
							<Input
								autoFocus
								value={this.state.idText}
								onChangeText={text => this.setState({ ...this.state, idText: text })}
							/>
							{this.state.idError && <Icon active name="ios-close-circle" />}
							{this.state.idSuccess && <Icon active name="ios-checkmark-circle" />}
						</Item>
						<Item
							error={this.state.passwordError}
							success={this.state.passwordSuccess}
							style={{ marginLeft: 0 }}
							floatingLabel
						>
							<Label>Password</Label>
							<Input
								value={this.state.passwordText}
								secureTextEntry
								onChangeText={text => this.setState({ ...this.state, passwordText: text })}
							/>
							{this.state.passwordError && <Icon active name="ios-close-circle" />}
							{this.state.passwordSuccess && <Icon active name="ios-checkmark-circle" />}
						</Item>
						<ListItem
							onPress={() => this.toggleSave()}
							style={{
								borderBottomWidth: 0.5, marginLeft: 0, marginRight: 0, paddingRight: 8,
							}}
						>
							<Body>
								<Text style={{ marginLeft: 0 }}>Save password?</Text>
							</Body>
							<CheckBox
								checked={this.state.saveLogin}
								onPress={() => this.toggleSave()}
							/>
						</ListItem>
					</Form>
					<View style={{ height: 25 }} />
					<Button block onPress={() => this.doLogin()}>
						<Text>Login</Text>
					</Button>
				</View>}
				<WebView
					url="https://teaching.brunel.ac.uk/SWS-1718/login.aspx"
					navigation={this.props.navigation}
					ref={webView => this.webView = webView}
					onLoad={this.onLoad}
				/>
			</ScreenContainer>
		);
	}
}

Login.propTypes = {
	navigation: PropTypes.any,
};
