import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { Body, H1, Button, Text } from 'native-base';
import ScreenContainer from '../components/ScreenContainer';

const Settings = props => (
	<ScreenContainer title="Settings" icon="ios-calendar" navigation={props.navigation} padder logo>
		<Body>
			<H1>Settings</H1>
		</Body>
		<View style={{ height: 25 }} />
		<Button
			block
			onPress={() => {
				global.storage.remove({
					key: 'LoginDetails',
				});
			}}
		>
			<Text>Clear login details</Text>
		</Button>
		<Button
			style={{ marginTop: 25 }}
			block
			onPress={() => {
				global.storage.remove({
					key: 'Timetable',
				});
			}}
		>
			<Text>Clear timetable</Text>
		</Button>
	</ScreenContainer>
);

Settings.propTypes = {
	navigation: PropTypes.any,
};

export default Settings;
