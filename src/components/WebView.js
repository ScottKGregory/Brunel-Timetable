import React from 'react';
import PropTypes from 'prop-types';
import { WebView } from 'react-native';

import { injectScript } from '../helpers/GetTimetable';

const { Dimensions } = require('react-native');

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;


class WebViewBridge extends React.Component {
	constructor(props) {
		super(props);

		this.doLogin = this.doLogin.bind(this);
		this.getHtml = this.getHtml.bind(this);
		this.getPageTitle = this.getPageTitle.bind(this);
		this.getCourseList = this.getCourseList.bind(this);
		this.getLevelList = this.getLevelList.bind(this);
		this.executeScript = this.executeScript.bind(this);
		this.onMessage = this.onMessage.bind(this);
		this.setLevel = this.setLevel.bind(this);
		this.setForm = this.setForm.bind(this);
		this.selectDefaults = this.selectDefaults.bind(this);
	}

	onMessage(event) {
		const message = JSON.parse(event.nativeEvent.data);
		switch (message.type) {
			case 'getPageTitle':
				this.props.getPageTitleCallback(message.message);
				break;
			case 'getCourseList':
				this.props.getCourseListCallback(message.message);
				break;
			case 'getLevelList':
				this.props.getLevelListCallback(message.message);
				break;
			case 'getTimetable':
				this.props.getTimetableCallback(message.message);
				break;
			default:
				console.log('Unknown message');
				break;
		}
	}

	getHtml() {
		this.executeScript('window.postMessage(JSON.stringify({type: \'getHtml\', message: document.body.outerHTML}));');
	}

	getPageTitle() {
		this.executeScript('window.postMessage(JSON.stringify({type: \'getPageTitle\', message: document.getElementById(\'tFilterTitle\').innerText}));');
	}

	getCourseList() {
		this.executeScript(`const select = document.getElementById('dlObject');
		let output = [];
		for (let i = 0; i < select.options.length; i += 1) {
			output.push({ value: select.options[i].value, label: select.options[i].text });
		}
		window.postMessage(JSON.stringify({type: 'getCourseList', message: output}));`);
	}

	getLevelList() {
		this.executeScript(`const select = document.getElementById('dlFilter');
		let output = [];
		for (let i = 0; i < select.options.length; i += 1) {
			output.push({ value: select.options[i].value, label: select.options[i].text });
		}
		window.postMessage(JSON.stringify({type: 'getLevelList', message: output}));`);
	}

	setLevel(level) {
		this.executeScript(`document.getElementById('dlFilter').value = '${level}';
	__doPostBack('dlFilter','');`);
	}

	setForm(level, course) {
		this.executeScript(`
		document.getElementById('dlObject').value = '${course}';
		document.getElementById('lbWeeks').selectedIndex = 2;
		document.getElementById('lbDays').selectedIndex = 1;
		document.getElementById('dlType').selectedIndex = 1;
		document.getElementById('bGetTimetable').click();`);
	}

	getTimetable() {
		this.executeScript(injectScript);
	}

	selectDefaults() {
		this.executeScript('');
	}

	clickElement(id) {
		this.executeScript(`document.getElementById('${id}').click();`);
	}

	doLogin(id, password) {
		this.executeScript(`document.getElementById('tUserName').value = '${id}';document.getElementById('tPassword').value = '${password}';document.getElementById('bLogin').click();`);
	}

	executeScript(code) {
		this.webView.postMessage(code);
	}

	render() {
		return (
			<WebView
				style={{
					height: deviceHeight / 2,
					width: deviceWidth - 18,
					flex: 1,
					marginTop: 10,
					display: 'none',
				}}
				javaScriptEnabled
				domStorageEnabled
				ref={webView => this.webView = webView}
				source={{ uri: this.props.url }}
				injectedJavaScript={'document.addEventListener("message", function(e){ eval(e.data); }, false);'}
				onMessage={event => this.onMessage(event)}
				onNavigationStateChange={e => this.setState({ ...this.state, navState: e })}
				onLoadEnd={() => this.props.onLoad(this.state.navState.url)}
			/>
		);
	}
}

WebViewBridge.propTypes = {
	url: PropTypes.string,
	onLoad: PropTypes.func,
	getPageTitleCallback: PropTypes.func,
	getCourseListCallback: PropTypes.func,
	getLevelListCallback: PropTypes.func,
	getTimetableCallback: PropTypes.func,
};

export default WebViewBridge;
