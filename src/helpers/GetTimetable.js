import { DOMParser } from 'react-native-html-parser';

export const injectScript = 'const timetable = document.getElementsByTagName(\'body\')[0].outerHTML; window.postMessage(JSON.stringify({ type: \'getTimetable\', message: timetable }));';

const weekDays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

const weekNumbers = ['18/09/2017', '25/09/2017', '02/10/2017', '09/10/2017', '16/10/2017', '23/10/2017', '30/10/2017', '06/11/2017', '13/11/2017', '20/11/2017', '27/11/2017', '04/12/2017', '11/12/2017', '18/12/2017', '25/12/2017', '01/01/2018', '08/01/2018', '15/01/2018', '22/01/2018', '29/01/2018', '05/02/2018', '12/02/2018', '19/02/2018', '26/02/2018', '05/03/2018', '12/03/2018', '19/03/2018', '26/03/2018', '02/04/2018', '09/04/2018', '16/04/2018', '23/04/2018', '30/04/2018', '07/05/2018', '14/05/2018', '21/05/2018', '28/05/2018', '04/06/2018', '11/06/2018', '18/06/2018', '25/06/2018', '02/07/2018', '09/07/2018', '16/07/2018', '23/07/2018', '30/07/2018', '06/08/2018', '13/08/2018', '20/08/2018', '27/08/2018', '03/09/2018', '10/09/2018'];

function stringArrayToInt(stringArray) {
	const intArray = [];
	Object.keys(stringArray).forEach((key) => {
		intArray.push(parseInt(stringArray[key], 10));
	});
	return intArray;
}

function weekNumToDate(weekNum, weekDay) {
	const dateParts = weekNumbers[weekNum].split('/');
	const weekStartDate = new Date(dateParts[2], dateParts[1], dateParts[0]);
	const dayOfWeek = new Date(
		+weekStartDate.getFullYear(),
		weekStartDate.getMonth(),
		weekStartDate.getDate() + weekDay,
	);
	return `${dayOfWeek.getDate()}/${dayOfWeek.getMonth()}/${dayOfWeek.getFullYear()}`;
}

function formatWeeks(weeks, weekDay) {
	const formattedWeeks = [];
	const splitWeeks = weeks.split(', ');
	Object.keys(splitWeeks).forEach((key) => {
		const rangeObject = {};
		rangeObject.start = {};
		rangeObject.end = {};
		const splitRange = stringArrayToInt(splitWeeks[key].split('-'));
		rangeObject.start.date = weekNumToDate(splitRange[0], weekDay);
		if (splitRange[1]) {
			rangeObject.end.date = weekNumToDate(splitRange[1], weekDay);
			[rangeObject.start.num, rangeObject.end.num] = splitRange;
		} else {
			rangeObject.end.date = weekNumToDate(splitRange[0], weekDay);
			[rangeObject.end.num] = splitRange;
			[rangeObject.start.num] = splitRange;
		}
		splitWeeks[key] = rangeObject;
		formattedWeeks.push(rangeObject);
	});
	return formattedWeeks;
}

function formatStaff(staff) {
	const splitStaff = staff.split(',');
	const staffArray = [];
	Object.keys(splitStaff).forEach((key) => {
		if (key % 2 === 0) {
			const staffObject = {};
			staffObject.surname = splitStaff[key];
			const nextName = parseInt(key, 10) + 1;
			staffObject.firstname = splitStaff[nextName];
			staffArray.push(staffObject);
		}
	});
	return staffArray;
}

function formatType(type) {
	switch (type) {
		case 'Lec/Sem': return 'Lecture/Seminar';
		case 'Lec': return 'Lecture';
		case 'Sem': return 'Seminar';
		case 'Comp Lab': return 'Computer Lab';
		case 'Lab': return 'Lab';
		case 'Tut': return 'Tutorial';
		default: return type;
	}
}

function formatRow(row, weekDay) {
	const formattedRow = row;
	formattedRow.weeks = formatWeeks(row.weeks, weekDay);
	formattedRow.staff = formatStaff(row.staff);
	formattedRow.type = formatType(row.type);
	return formattedRow;
}

function getTimetable(html) {
	const doc = new DOMParser().parseFromString(html, 'text/html');
	const tables = doc.getElementsByClassName('spreadsheet');
	const timetable = {};

	for (let i = 0; i < 7; i += 1) {
		timetable[weekDays[i]] = [];
		const outputTable = {};
		outputTable.rows = [];
		if (tables[i].rows[0] !== undefined) {
			const table = tables[i];

			const headings = [];
			Object.keys(table.rows[0].cells).forEach((key) => {
				headings.push(table.rows[0].cells[key].innerText);
			});

			for (let j = 1; j < table.rows.length; j += 1) {
				const row = {};
				Object.keys(table.rows[j].cells).forEach((key) => {
					row[headings[key].replace(/[^a-z0-9+]+/gi, '').toLowerCase()] = table.rows[j].cells[key].innerText;
				});
				timetable[weekDays[i]].push(formatRow(row, i));
			}
		} else {
			timetable[weekDays[i]].push({
				activity: '',
				description: '',
				start: '',
				end: '',
				weeks: [
					{
						start: {
							date: '01/01/2018',
							num: 0,
						},
						end: {
							date: '01/01/2018',
							num: 0,
						},
					},
				],
				room: '',
				staff: [
					{
						surname: '',
						firstname: '',
					},
				],
				type: '',
			});
		}
	}
	return timetable;
}

export function parse(timetable) {
	const tt = getTimetable(timetable);
	console.log(tt);
}
